<?php

/**
 * Email provider for send emails
 *
 * @category Core
 * @package Core_Tool
 * @author V.Leontiev
 * @license New BSD License
 *
 * @version  $Id$
 */
class Core_Tool_Project_Provider_Email
    extends Core_Tool_Project_Provider_Abstract
        implements Zend_Tool_Framework_Provider_Pretendable
{
    
    /**
     * Send emails 
     * 
     */
    public function send()
    {
        
    }
}

