<?php
/**
 *
 * @author vadim
 */
interface Core_Acl_Adapter_Abstract
{
    /**
     * Get options
     *
     * @return array
     */
    public function getOptions();
}

