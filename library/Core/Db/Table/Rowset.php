<?php

/**
 * Class extends for Core_Db_Table_Rowset_Abstract
 *
 * @category Core
 * @package Core_Db
 * @subpackage Table
 * @license New BSD
 * @author V.Leontiev <vadim.leontiev@gmail.com>
 */
class Core_Db_Table_Rowset extends Core_Db_Table_Rowset_Abstract
{
    
}
