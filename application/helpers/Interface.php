<?php

/**
 * Interface for aplication helpers
 *
 * @category Application
 * @package Application_Helper
 * @author Vadim Leontiev <vadim.leontiev@gmail.com>
 * @see https://bitbucket.org/newage/clean-zfext
 * @since php 5.1 or higher
 */
interface Application_Helper_Interface
{
    public function init();
}

